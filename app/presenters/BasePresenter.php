<?php

namespace App\Presenters;

use Nette;
use App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	public function createTemplate($class = NULL)
	{
		$template = parent::createTemplate($class);
		$template->addFilter('path', function($name){
			return '/data/files/' . $name ;
		});

		return $template;
	}
	
	public function beforeRender()
	{
		$imagePath = $this->context->parameters['imagePath'];
		$debug = $this->context->parameters['productionMode'];
		$this->template->imagePath = $imagePath;
		$this->template->debugMode = $debug;
	}

}
